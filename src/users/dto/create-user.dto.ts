export class CreateUserDto {
  fullName: string;
  email: string;
  password: string;
  gender: string;
  roles: string;
  image: string;
}
